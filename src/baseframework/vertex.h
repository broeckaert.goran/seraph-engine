#pragma once

struct Vertex {
    float x;
    float y;
    float z;

    float tx;
    float ty;

    float nx;
    float ny;
    float nz;
};
