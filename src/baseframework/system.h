#pragma once

#include "observer.h"
#include "subject.h"

class System
    : public Observer
    , public Subject
{
public:
    virtual ~System() = default;

    /**
     * Method called before running. This is important to setup your system
     * before running.
     *
     * This method is **not mandatory**.
     */
    virtual void build() {}

    /**
     * Method called every update cycle. The delta is the amount of time passed
     * from the previous update cycle.
     *
     * This method is **mandatory** to inherit
     *
     * @param delta The amount of time compared to the previous update cycle.
     */
    virtual void update(double delta) = 0;

    /**
     * Method called before the destructor. This is called when the update loop
     * is terminated.
     *
     * This method is **not mandatory** to inherit.
     */
    virtual void terminate() {}
};
