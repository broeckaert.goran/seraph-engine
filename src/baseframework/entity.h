#pragma once

#include <optional>
#include <set>

#include "component.h"

class Entity
{
public:
    ~Entity()
    {
        for (auto component : mComponents) delete component;
    }

    template <typename T>
    T* getComponent()
    {
        for (auto component : mComponents) {
            auto componentCasted = dynamic_cast<T*>(component);
            if (componentCasted != nullptr) {
                return componentCasted;
            }
        }

        return nullptr;
    }

    template <typename T>
    bool hasComponent()
    {
        T* component = getComponent<T>();
        return component == nullptr;
    }

    void addComponent(Component* component) { mComponents.insert(component); }

private:
    std::set<Component*> mComponents;
};
