#include "rendersystem.h"

#include <GL/glew.h>

#include "application.h"
#include "cameracomponent.h"
#include "fbo.h"
#include "modelcomponent.h"
#include "shader.h"
#include "transformcomponent.h"

void RenderSystem::build() {}

void RenderSystem::update(double delta)
{
    auto app = Application::instance();

    auto cameras =
        app->getEntitiesWithComponents<CameraComponent, TransformComponent>();
    auto entitiesToRender =
        app->getEntitiesWithComponents<ModelComponent, TransformComponent>();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);

    for (auto camera : cameras) {
        auto cameraComponent = camera->getComponent<CameraComponent>();
        cameraComponent->framebuffer()->bind();

        auto shader = cameraComponent->shader();
        shader->start();
        // TODO: these parameters should be set in the shader itself.
        // shader.setUniformInt("myTexture", 0);
        // TODO: these parameters should be set in the shader itself.
        // shader.setUniformMat4("view",
        // camera->getComponent<TransformComponent>().value()->getInverseMatrix());

        for (auto entity : entitiesToRender) {
            // TODO: these parameters should be set in the shader itself.
            // shader.setUniformMat4("model",
            // entity->getComponent<TransformComponent>().value()->getTransformationMatrix());
            auto model = entity->getComponent<ModelComponent>();

            for (auto mesh : model->getMeshes()) {
                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, std::get<2>(mesh));

                glBindVertexArray(std::get<0>(mesh));

                glDrawElements(GL_TRIANGLES, std::get<1>(mesh), GL_UNSIGNED_INT,
                               0);
            }
        }

        shader->stop();
    }

    glDisable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
}
