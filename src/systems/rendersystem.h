#pragma once

#include <string>

#include "system.h"

class RenderSystem : public System
{
public:
    void build() override;
    void update(double delta) override;
};
