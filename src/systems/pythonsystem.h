#pragma once

#include <list>
#include <string>
#include <tuple>

#include "system.h"

class PythonSystem : public System
{
public:
    PythonSystem() = default;
    virtual ~PythonSystem() = default;

    void build() override;
    void update(double delta) override;
};
