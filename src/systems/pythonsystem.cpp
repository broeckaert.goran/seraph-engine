#include "pythonsystem.h"

#include <iostream>

#include "logger.h"
#include "private/application_python.h"
#include "private/entity_python.h"
#include "private/logger_python.h"

namespace py = pybind11;

static void setPythonFolder()
{
    auto sysModule = py::module::import("sys");
    sysModule.attr("path").attr("insert")(1, "python");
}

void PythonSystem::build()
{
    py::scoped_interpreter guard;
    try {
        setPythonFolder();

        auto coreModule = py::module::import("core");
        coreModule.attr("build")();
    } catch (const std::runtime_error& re) {
        LOGE(re.what());
    }
}

void PythonSystem::update(double delta)
{
    py::scoped_interpreter guard;
    try {
        setPythonFolder();

        auto coreModule = py::module::import("core");
        coreModule.attr("update")(delta);
    } catch (const std::runtime_error& re) {
        LOGE(re.what());
    }
}
