cmake_minimum_required(VERSION 3.22)
project(systems CXX)
set(CMAKE_CXX_STANDARD 23)

set(SOURCES
    application.cpp
    pythonsystem.cpp
    rendersystem.cpp
)

set(HEADERS
    application.h
    pythonsystem.h
    rendersystem.h
)

add_library(${PROJECT_NAME} SHARED ${SOURCES} ${HEADERS})
set_target_properties(${PROJECT_NAME} PROPERTIES LINKER_LANGUAGE CXX)

target_include_directories(${PROJECT_NAME} PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
    $<INSTALL_INTERFACE:include/>)
target_link_libraries(${PROJECT_NAME}
    PRIVATE pybind11::embed

    PUBLIC baseframework
    PUBLIC components
    PUBLIC renderer
    PUBLIC logger
)

install(FILES ${HEADERS} DESTINATION include COMPONENT dev)
install(TARGETS ${PROJECT_NAME}
    EXPORT "SeraphEngineTargets"
    RUNTIME DESTINATION bin COMPONENT libraries
    LIBRARY DESTINATION lib COMPONENT libraries
    ARCHIVE DESTINATION lib/static COMPONENT libraries)
export(TARGETS ${PROJECT_NAME} APPEND FILE "${CMAKE_BINARY_DIR}/SeraphEngineTargets.cmake")
