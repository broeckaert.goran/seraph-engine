#include "application.h"

#include <GLFW/glfw3.h>

#include <iostream>
#include <stdexcept>
#include <thread>

#define TARGET_UPS 120.0
#define TARGET_FPS 60.0

Application::Application()
    : mPastTime(std::chrono::system_clock::now()), mTimer(mPastTime)
{
}

Application::~Application()
{
    for (auto entity : mEntities) delete entity;

    for (auto system : mSystems) delete system;
}

void Application::stop() { mRunning = false; }

int Application::run()
{
    mRunning = true;

    for (auto system : mSystems) system->build();

    while (mRunning) {
        std::chrono::duration<double> delta = mTimer - mPastTime;

        while (delta.count() < (1 / TARGET_UPS)) {
            std::this_thread::sleep_for(
                std::chrono::duration<double>(1 / TARGET_UPS) - delta);

            mTimer = std::chrono::system_clock::now();
            delta = mTimer - mPastTime;
        }

        for (auto system : mSystems) system->update(delta.count());

        mPastTime = mTimer;
    }

    return 0;
}

void Application::addSystem(System* system) { mSystems.insert(system); }

void Application::removeSystem(System* system) { mSystems.erase(system); }

void Application::addEntity(Entity* entity) { mEntities.insert(entity); }

void Application::removeEntity(Entity* entity) { mEntities.erase(entity); }
