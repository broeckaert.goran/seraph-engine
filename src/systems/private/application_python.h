#pragma once

#include <pybind11/embed.h>
#include <pybind11/stl.h>

#include "application.h"
#include "enemycomponent.h"
#include "modelcomponent.h"
#include "transformcomponent.h"

namespace py = pybind11;

PYBIND11_EMBEDDED_MODULE(application, m)
{
    py::class_<Application, std::unique_ptr<Application, py::nodelete>>(
        m, "Application")
        .def(py::init([]() {
            return std::unique_ptr<Application, py::nodelete>(
                Application::instance());
        }))
        .def("get_entities",
             &Application::getEntitiesWithComponents<TransformComponent,
                                                     ModelComponent>)
        .def("get_enemies",
             &Application::getEntitiesWithComponents<
                 TransformComponent, ModelComponent, EnemyComponent>);
}
