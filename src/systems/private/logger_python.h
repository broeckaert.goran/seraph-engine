#pragma once

#include <pybind11/embed.h>
#include <pybind11/pybind11.h>

#include "logger.h"

PYBIND11_EMBEDDED_MODULE(logger, m)
{
    m.def("log_error", [](const std::string& message) {
        Logger::instance()->error(message);
    });
    m.def("log_warn", [](const std::string& message) {
        Logger::instance()->warn(message);
    });
    m.def("log_info", [](const std::string& message) {
        Logger::instance()->info(message);
    });
    m.def("log_debug", [](const std::string& message) {
        Logger::instance()->debug(message);
    });
    m.def("log_trace", [](const std::string& message) {
        Logger::instance()->trace(message);
    });
}
