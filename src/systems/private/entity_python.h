#pragma once

#include <pybind11/embed.h>
#include <pybind11/stl.h>

#include "entity.h"

namespace py = pybind11;

PYBIND11_EMBEDDED_MODULE(entities, m)
{
    py::class_<Entity>(m, "Entity").def(py::init<>());
}
