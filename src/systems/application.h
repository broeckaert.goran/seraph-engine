#pragma once

#include <chrono>
#include <set>

#include "entity.h"
#include "singleton.h"
#include "system.h"

// TODO: Add a configuration step to make so we can have a global configuration
// file.
class Application : public Singleton<Application>
{
    friend class Singleton<Application>;

public:
    virtual ~Application();

    virtual void stop();
    virtual int run();

    /**
     * @brief Gets all the entities that contains a list of components.
     *
     * @tparam T Components the entity has to contain.
     * @return A set that contains all the entities filtered with the components
     * requested.
     */
    template <typename... T>
    std::set<Entity*> getEntitiesWithComponents()
    {
        std::set<Entity*> entitiesWithComponents;
        for (auto entity : mEntities)
            if (hasEntityComponents<T...>(entity))
                entitiesWithComponents.insert(entity);

        return entitiesWithComponents;
    }

    void addSystem(System* system);
    void removeSystem(System* system);

    template <typename T>
    T* getSystem()
    {
        for (auto system : mSystems)
            if (auto castedSystem = dynamic_cast<T*>(system))
                return castedSystem;

        return nullptr;
    }

    void addEntity(Entity* entity);
    void removeEntity(Entity* entity);

protected:
    Application();

    int mUpdates = 0, mFrames = 0;

    std::chrono::time_point<std::chrono::high_resolution_clock> mPastTime;
    std::chrono::time_point<std::chrono::high_resolution_clock> mTimer;

    std::set<System*> mSystems;
    std::set<Entity*> mEntities;

private:
    volatile bool mRunning = false;

    /**
     * @brief This makes a list of all the results of the different components
     * requested. After we get all the results for all the components, we check
     * if the entity has all the components. If a entity is missing 1 or more
     * components we return false.
     *
     * @tparam T Components the entity has to contain.
     * @param entity The entity for which we have to check whether contains all
     * the components.
     * @return true if the entity contains all the components requested.
     */
    template <typename... T>
    bool hasEntityComponents(Entity* entity)
    {
        return ((hasEntityComponents<T>(entity) && ...));
    }
};
