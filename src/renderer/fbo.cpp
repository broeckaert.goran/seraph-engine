#include "fbo.h"

#include <GL/glew.h>

#include <stdexcept>

Fbo* Fbo::getFramebuffer()
{
    static Fbo framebuffer;
    return &framebuffer;
}

Fbo::Fbo() : mFramebuffer(0) {}

Fbo::Fbo(int width, int height) : mWidth(width), mHeight(height)
{
    glGenFramebuffers(1, &mFramebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, mFramebuffer);
    glDrawBuffer(GL_COLOR_ATTACHMENT0);
}

Fbo::~Fbo()
{
    glDeleteFramebuffers(1, &mFramebuffer);

    for (auto& [type, texture] : mTextureMap) {
        glDeleteTextures(1, &texture);
    }

    mTextureMap.clear();
}

void Fbo::addColorTextureAttachment()
{
    if (mTextureMap.find(COLOR_TEXTURE) != mTextureMap.end()) {
        throw std::logic_error(
            "[#000002] Adding a color attachment when there is already one "
            "created. Multiple color attachments aren't supported yet.");
    }

    unsigned int texture;

    bind();

    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGBA8, mWidth, mHeight);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texture, 0);

    mTextureMap[COLOR_TEXTURE] = texture;
}

void Fbo::addDepthTextureAttachment()
{
    if (mTextureMap.find(DEPTH_TEXTURE) != mTextureMap.end()) {
        throw std::logic_error(
            "[#000002] Having multiple depth textures is not possible.");
    }

    unsigned int texture;

    bind();

    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexStorage2D(GL_TEXTURE_2D, 1, GL_DEPTH_COMPONENT32F, mWidth, mHeight);

    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, texture, 0);

    mTextureMap[DEPTH_TEXTURE] = texture;
}

void Fbo::bind() const
{
    glBindFramebuffer(GL_FRAMEBUFFER, mFramebuffer);
    glViewport(0, 0, mWidth, mHeight);
}
