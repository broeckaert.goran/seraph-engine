#pragma once

#include <string>

class BaseShader
{
public:
    BaseShader() = delete;
    BaseShader(const BaseShader& other) = delete;
    BaseShader(const BaseShader&& dest) = delete;

    virtual ~BaseShader();

    void start() const;
    void stop() const;

protected:
    BaseShader(const std::string& vertexPath, const std::string& fragmentPath);
    BaseShader(const std::string& vertexPath, const std::string& fragmentPath,
               const std::string& geometryPath);

private:
    enum CompileType {
        PROGRAM,
        VERTEX,
        FRAGMENT,
        GEOMETRY,
    };

    void checkCompileErrors(unsigned int shader, CompileType type);
    std::string readShader(const std::string& programPath);

private:
    unsigned int mProgramID;
};
