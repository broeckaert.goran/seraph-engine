#pragma once

#include <glm/glm.hpp>

class BaseProjection
{
public:
    virtual ~BaseProjection() = default;

    virtual glm::mat4 populateMatrix() const = 0;
};

class PerspectiveProjection : public BaseProjection
{
public:
    // TODO: Get the parameters from the configuration file. (Maybe not near and
    // far plane, but could be cool if it's possible for the player to configure
    // these parameters)
    PerspectiveProjection(float fov, float width, float height, float near,
                          float far);
    virtual glm::mat4 populateMatrix() const override;

private:
    float mFOV, mWidth, mHeight, mNear, mFar;
};
