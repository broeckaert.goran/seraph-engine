#include "projection.h"

#include <glm/gtc/matrix_transform.hpp>

PerspectiveProjection::PerspectiveProjection(float fov, float width,
                                             float height, float near,
                                             float far)
    : mFOV(fov), mWidth(width), mHeight(height), mNear(near), mFar(far)
{
}

glm::mat4 PerspectiveProjection::populateMatrix() const
{
    return glm::perspective(glm::radians(mFOV), mWidth / mHeight, mNear, mFar);
}
