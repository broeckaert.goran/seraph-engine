#include "shader.h"

#include <GL/glew.h>

#include <fstream>
#include <sstream>
#include <stdexcept>

#include "fileutility.h"

BaseShader::BaseShader(const std::string& vertexPath,
                       const std::string& fragmentPath)
{
    mProgramID = glCreateProgram();

    std::string vertexShader = readShader(vertexPath);
    std::string fragmentShader = readShader(fragmentPath);

    const char* vertexCode = vertexShader.c_str();
    const char* fragmentCode = fragmentShader.c_str();

    unsigned int vertexID = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexID, 1, &vertexCode, NULL);
    unsigned int fragmentID = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentID, 1, &fragmentCode, NULL);

    glCompileShader(vertexID);
    checkCompileErrors(vertexID, VERTEX);
    glCompileShader(fragmentID);
    checkCompileErrors(fragmentID, FRAGMENT);

    glAttachShader(mProgramID, vertexID);
    glAttachShader(mProgramID, fragmentID);

    glLinkProgram(mProgramID);
    checkCompileErrors(mProgramID, PROGRAM);

    glDeleteShader(vertexID);
    glDeleteShader(fragmentID);
}

BaseShader::BaseShader(const std::string& vertexPath,
                       const std::string& fragmentPath,
                       const std::string& geometryPath)
{
    mProgramID = glCreateProgram();

    std::string vertexShader = readShader(vertexPath);
    std::string fragmentShader = readShader(fragmentPath);
    std::string geometryShader = readShader(geometryPath);

    const char* vertexCode = vertexShader.c_str();
    const char* fragmentCode = fragmentShader.c_str();
    const char* geometryCode = geometryShader.c_str();

    unsigned int vertexID = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexID, 1, &vertexCode, NULL);
    unsigned int fragmentID = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentID, 1, &fragmentCode, NULL);
    unsigned int geometryID = glCreateShader(GL_GEOMETRY_SHADER);
    glShaderSource(geometryID, 1, &geometryCode, NULL);

    glCompileShader(vertexID);
    checkCompileErrors(vertexID, VERTEX);
    glCompileShader(fragmentID);
    checkCompileErrors(fragmentID, FRAGMENT);
    glCompileShader(geometryID);
    checkCompileErrors(geometryID, GEOMETRY);

    glAttachShader(mProgramID, vertexID);
    glAttachShader(mProgramID, fragmentID);
    glAttachShader(mProgramID, geometryID);

    glLinkProgram(mProgramID);
    checkCompileErrors(mProgramID, PROGRAM);

    glDeleteShader(vertexID);
    glDeleteShader(fragmentID);
    glDeleteShader(geometryID);
}

BaseShader::~BaseShader() { glDeleteProgram(mProgramID); }

void BaseShader::start() const { glUseProgram(mProgramID); }

void BaseShader::stop() const { glUseProgram(0); }

void BaseShader::checkCompileErrors(unsigned int shader,
                                    BaseShader::CompileType type)
{
}

std::string BaseShader::readShader(const std::string& programPath)
{
    if (!fileExists(programPath)) {
        throw std::runtime_error(
            "[E] BaseShader::readShader: Could not find shader path <" +
            programPath + ">");
    }

    std::string shader;

    // Make sure we aren't using these resources after we're done reading them
    {
        std::ifstream stream(programPath, std::ifstream::in);

        std::stringstream program;
        program << stream.rdbuf();

        shader = program.str();
    }

    return shader;
}
