#pragma once

#include <map>

enum TextureType {
    COLOR_TEXTURE,
    DEPTH_TEXTURE,
};

class Fbo
{
public:
    static Fbo* getFramebuffer();

    Fbo(int width, int height);
    Fbo(const Fbo& other) = delete;
    Fbo(const Fbo&& other) = delete;
    virtual ~Fbo();

    void addColorTextureAttachment();
    void addDepthTextureAttachment();

    void bind() const;

private:
    Fbo();

    int mWidth, mHeight;
    unsigned int mFramebuffer;
    std::map<enum TextureType, unsigned int> mTextureMap;
};
