#pragma once

#include "shader.h"

class GuiShader : public BaseShader
{
public:
    GuiShader()
        : BaseShader("res/shaders/guivertex.glsl",
                     "res/shaders/guifragment.glsl")
    {
    }

    ~GuiShader() override = default;
};
