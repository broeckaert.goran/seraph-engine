#include "eventloop.h"

#include <sys/epoll.h>
#include <sys/eventfd.h>

using namespace std::chrono_literals;

constexpr std::size_t reserve_size = 8;

auto Eventloop::Scheduled::await_suspend(std::coroutine_handle<> handle) -> void
{
    m_eventloop.m_size.fetch_add(1, std::memory_order::release);

    std::scoped_lock lk{m_eventloop.m_mutexScheduledTasks};
    m_eventloop.m_scheduledTasks.emplace_back(handle);

    // Triger epoll with a new event to handle the scheduled task, if it hasn't
    // been triggered yet.
    bool expected{false};
    if (m_eventloop.m_scheduled_triggered.compare_exchange_strong(
            expected, true, std::memory_order::release,
            std::memory_order::relaxed)) {
        eventfd_t value{1};
        eventfd_write(m_eventloop.m_scheduled_fd, value);
    }
}

Eventloop::Eventloop()
    : m_epoll_fd(epoll_create1(EPOLL_CLOEXEC))
    , m_scheduled_fd(eventfd(0, EFD_CLOEXEC | EFD_NONBLOCK))
{
    m_tasks.resize(reserve_size);

    for (auto i = 0; i < reserve_size; ++i)
        m_taskIndices.emplace_back(i);
    m_tasksPosition = m_taskIndices.begin();

    struct epoll_event e {
        .events = EPOLLIN
    };
    e.data.ptr = const_cast<void*>(scheduled_object_ptr);
    epoll_ctl(m_epoll_fd, EPOLL_CTL_ADD, m_scheduled_fd, &e);
}

Eventloop::~Eventloop()
{
    if (m_epoll_fd != -1) {
        ::close(m_epoll_fd);
        m_epoll_fd = -1;
    }

    if (m_scheduled_fd != -1) {
        ::close(m_scheduled_fd);
        m_scheduled_fd = -1;
    }
}

auto Eventloop::schedule() -> Eventloop::Scheduled { return Scheduled(*this); }

auto Eventloop::schedule(Task<void>&& task) -> void
{
    std::scoped_lock lk{m_mutexTasks};

    if (m_tasksPosition == m_taskIndices.end()) {
        m_tasksPosition = grow();
    }

    auto index = *m_tasksPosition;
    m_tasks[index] = makeScheduledTask(std::move(task), m_tasksPosition);

    m_tasksPosition = std::next(m_tasksPosition);

    m_tasks[index].resume();
}

auto Eventloop::schedule(int fd, enum Operation op, std::chrono::milliseconds timeout) -> Task<enum Event::Status>
{
    m_size.fetch_add(1, std::memory_order::release);

    bool timer = (timeout > 0ms);
    // TODO: add timeout to the event.

    Event event(fd);

    struct epoll_event e{};
    e.events = static_cast<uint32_t>(op) | EPOLLONESHOT | EPOLLRDHUP;
    e.data.ptr = &event;

    if (epoll_ctl(m_epoll_fd, EPOLL_CTL_ADD, fd, &e) == -1) {
        // TODO: ERROR
    }

    co_await event;

    m_size.fetch_sub(1, std::memory_order::release);
    co_return event.m_status;
}

auto Eventloop::processEvents(std::chrono::milliseconds timeout) -> std::size_t
{
    auto event_count = epoll_wait(m_epoll_fd, m_events.data(), m_events.size(),
                                  timeout.count());
    if (event_count > 0) {
        for (auto i = 0; i < event_count; ++i) {
            struct epoll_event& event = m_events[i];
            void* handle = event.data.ptr;

            if (handle == shutdown_object_ptr) {
                // Do nothing, just have to wake up to shutdown.
            } else if (handle == timer_object_ptr) {
                // Handle all the timers.
            } else if (handle == scheduled_object_ptr) {
                // Events added with schedule(Task<void>&&);
                handleScheduledTasks();
            } else {
                // Custom events.
                handleEvent(static_cast<Event*>(handle));
            }
        }
    }

    return size();
};

auto Eventloop::size() const noexcept -> std::size_t
{
    return m_size.load(std::memory_order::acquire);
}

auto Eventloop::grow() -> std::list<std::size_t>::iterator
{
    auto last = std::prev(m_tasksPosition);
    std::size_t new_size = m_tasks.size() * 2;

    for (auto i = m_tasks.size(); i < new_size; ++i)
        m_taskIndices.emplace_back(i);

    m_tasks.resize(new_size);

    return std::next(last);
}

auto Eventloop::makeScheduledTask(Task<void> task,
                                  std::list<std::size_t>::iterator pos)
    -> Task<void>
{
    // Make sure we are running on the Eventloop.
    co_await schedule();

    try {
        co_await task;
    } catch (const std::exception& exception) {
        // TODO: add error handling
    } catch (...) {
        // TODO: add error handling
    }

    // Garbage collect the task (move the finised coroutine iterator to the back
    // of the list).
    std::scoped_lock lk{m_mutexTasks};
    m_taskIndices.splice(m_taskIndices.end(), m_taskIndices, pos);

    m_size.fetch_sub(1, std::memory_order::relaxed);

    co_return;
}

auto Eventloop::handleScheduledTasks() -> void
{
    std::vector<std::coroutine_handle<>> tasks{};
    {
        std::scoped_lock lk{m_mutexScheduledTasks};
        tasks.swap(m_scheduledTasks);
    }

    for (auto& task : tasks)
        task.resume();
}

auto Eventloop::handleEvent(Event* event) -> void
{
    // Remove filedescriptor so we can keep poll in our coroutines.
    if (event->m_fd != -1)
        epoll_ctl(m_epoll_fd, EPOLL_CTL_DEL, event->m_fd, nullptr);

    event->m_status = Event::Status::EVENT;

    // Resume the task, since it has been triggered.
    event->m_handle.resume();
}
