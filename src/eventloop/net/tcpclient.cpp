#include "tcpclient.h"

#include <arpa/inet.h>
#include <sys/eventfd.h>
#include <unistd.h>

TcpClient::TcpClient(std::shared_ptr<Eventloop> eventloop,
                     enum net::Domain domain, enum net::Type type)
    : m_eventloop(eventloop)
    , m_fd(::socket(static_cast<int>(domain), static_cast<int>(type), 0))
    , m_domain(domain)
{
}

TcpClient::TcpClient(std::shared_ptr<Eventloop> eventloop, int fd,
                     enum net::Domain domain)
    : m_eventloop(eventloop)
    , m_fd(fd)
    , m_domain(domain)
{
}

TcpClient::~TcpClient()
{
    if (m_fd == -1) {
        ::close(m_fd);
        m_fd = -1;
    }
}

auto TcpClient::connect(const std::string& ip, uint16_t port) -> Task<bool>
{
    struct sockaddr_in server {
    };
    server.sin_family = static_cast<int>(m_domain);
    server.sin_port = htons(port);
    server.sin_addr = {net::ipFromString(ip, m_domain)};

    auto ret = ::connect(m_fd, (struct sockaddr*)&server, sizeof(server));
    if (ret == 0)
        co_return true;
    else
        co_return false;
    // TODO: Use coroutines to allow retries.
}

auto TcpClient::send(const std::string& data) -> std::size_t
{
    if (data.empty())
        return 0;

    return ::send(m_fd, data.data(), data.size(), 0);
}

auto TcpClient::read(std::string& buffer, std::size_t size) -> Task<std::size_t>
{
    co_await m_eventloop->schedule(m_fd, Eventloop::Operation::READ);

    co_return ::recv(m_fd, buffer.data(), size, 0);
}
