#pragma once

#include <arpa/inet.h>
#include <string>

namespace net
{
enum Domain {
    IPV4 = AF_INET,
    IPV6 = AF_INET6,
};

enum Type {
    TCP = SOCK_STREAM,
    UDP = SOCK_DGRAM,
};

inline auto ipFromString(const std::string& ip, enum Domain domain = IPV4)
    -> uint32_t
{
    uint32_t result{0};
    inet_pton(domain, ip.c_str(), &result);
    return result;
}
} // namespace net
