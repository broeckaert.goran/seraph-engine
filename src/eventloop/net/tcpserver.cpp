#include "tcpserver.h"
#include "net/netutils.h"

TcpServer::TcpServer(std::shared_ptr<Eventloop> eventloop,
                     const std::string& ip, uint16_t port,
                     enum net::Domain domain, enum net::Type type)
    : m_eventloop(eventloop)
    , m_ip(ip)
    , m_port(port)
    , m_domain(domain)
    , m_fd(::socket(static_cast<int>(domain), static_cast<int>(type), 0))
{
    int sock_opt{1};
    setsockopt(m_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &sock_opt, sizeof(sock_opt));

    struct sockaddr_in server{};
    server.sin_family = static_cast<int>(domain);
    server.sin_port = htons(port);
    server.sin_addr = {net::ipFromString(ip)};

    if (::bind(m_fd, (struct sockaddr*)&server, sizeof(server)) < 0)
        throw std::runtime_error("Unable to bind socket.");
}

TcpServer::~TcpServer()
{
    if (m_fd != -1) {
        ::close(m_fd);
        m_fd = -1;
    }
}

auto TcpServer::poll() -> Task<enum Event::Status>
{
    co_return co_await m_eventloop->schedule(m_fd, Eventloop::Operation::READ);
}

auto TcpServer::accept() -> std::unique_ptr<TcpClient>
{
    struct sockaddr_in client{};
    constexpr const int len = sizeof(struct sockaddr_in);

    int fd = ::accept(m_fd, (struct sockaddr*)&client, (socklen_t*)&len);

    return std::make_unique<TcpClient>(m_eventloop, fd, static_cast<enum net::Domain>(client.sin_family));
}
