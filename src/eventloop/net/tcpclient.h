#pragma once

#include "eventloop.h"
#include "net/netutils.h"

#include <memory>
#include <string>

class TcpClient
{
public:
    TcpClient(std::shared_ptr<Eventloop> eventloop, enum net::Domain domain,
              enum net::Type type);
    TcpClient(std::shared_ptr<Eventloop> eventloop, int fd,
              enum net::Domain domain);

    ~TcpClient();

    auto connect(const std::string& ip, uint16_t port) -> Task<bool>;
    auto send(const std::string& data) -> std::size_t;
    auto read(std::string& buffer, std::size_t size = 100) -> Task<std::size_t>;

private:
    std::shared_ptr<Eventloop> m_eventloop;

    int m_fd{-1};
    enum net::Domain m_domain;
};
