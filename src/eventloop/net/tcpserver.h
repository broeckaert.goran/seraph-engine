#pragma once

#include "eventloop.h"
#include "net/netutils.h"
#include "net/tcpclient.h"

#include <memory>

class TcpServer
{
public:
    TcpServer(std::shared_ptr<Eventloop> eventloop, const std::string& ip,
              uint16_t port, enum net::Domain domain = net::IPV4,
              enum net::Type type = net::TCP);
    ~TcpServer();

    auto poll() -> Task<enum Event::Status>;
    auto accept() -> std::unique_ptr<TcpClient>;

private:
    std::shared_ptr<Eventloop> m_eventloop;
    const std::string m_ip;
    const uint16_t m_port;
    const enum net::Domain m_domain;

    int m_fd{-1};
};
