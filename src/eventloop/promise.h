#pragma once

#include "awaitable.h"

#include <concepts>

// clang-format off
template<typename T, typename R>
concept promise = requires(T t)
{
    { t.get_return_object() } -> std::convertible_to<std::coroutine_handle<>>;
    { t.initial_suspend() } -> awaiter;
    { t.final_suspend() } -> awaiter;
    { t.yield_value() } -> awaitable;
}
&& requires(T t, R return_value)
{
    requires std::same_as<decltype(t.return_void()), void> ||
        std::same_as<decltype(t.return_value(return_value)), void> ||
        requires { t.yield_value(return_value); };
};
// clang-format on
