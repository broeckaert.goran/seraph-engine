#pragma once

#include "detail/event.h"
#include "task.h"

#include <array>
#include <atomic>
#include <chrono>
#include <cstddef>
#include <list>
#include <mutex>
#include <sys/epoll.h>
#include <vector>

class Eventloop
{
public:
    class Scheduled;
    friend class Scheduled;

    enum Operation {
        READ = EPOLLIN,
        WRITE = EPOLLOUT,
        READ_WRITE = EPOLLIN | EPOLLOUT,
    };

    class Scheduled
    {
        friend class Eventloop;
        explicit Scheduled(Eventloop& eventloop) noexcept
            : m_eventloop(eventloop)
        {
        }

    public:
        auto await_ready() -> bool { return false; }
        auto await_suspend(std::coroutine_handle<> handle) -> void;
        auto await_resume() -> void {}

        Scheduled() = delete;

    private:
        Eventloop& m_eventloop;
    };

    Eventloop();
    ~Eventloop();

    auto schedule() -> Eventloop::Scheduled;
    auto schedule(Task<void>&& task) -> void;
    [[nodiscard]] auto
    schedule(int fd, enum Operation op,
             std::chrono::milliseconds timeout = std::chrono::milliseconds{0})
        -> Task<enum Event::Status>;

    auto processEvents(std::chrono::milliseconds timeout =
                           std::chrono::milliseconds{0}) -> std::size_t;

    auto size() const noexcept -> std::size_t;
    auto empty() const noexcept -> bool { return size() == 0; }

private:
    auto grow() -> std::list<std::size_t>::iterator;
    auto makeScheduledTask(Task<void> task,
                           std::list<std::size_t>::iterator pos) -> Task<void>;

    auto handleScheduledTasks() -> void;
    auto handleEvent(Event* event) -> void;

private:
    static const constexpr int shutdown_object{0};
    static const constexpr void* shutdown_object_ptr = &shutdown_object;
    static const constexpr int timer_object{0};
    static const constexpr void* timer_object_ptr = &timer_object;
    static const constexpr int scheduled_object{0};
    static const constexpr void* scheduled_object_ptr = &scheduled_object;

    static const constexpr std::size_t max_events{16};

private:
    std::list<std::size_t> m_taskIndices;
    std::list<std::size_t>::iterator m_tasksPosition;

    std::vector<Task<void>> m_tasks;
    std::atomic<std::size_t> m_size;

    std::mutex m_mutexTasks;

    std::vector<std::coroutine_handle<>> m_scheduledTasks{};
    std::vector<std::coroutine_handle<>> m_resumeTasks{};
    std::mutex m_mutexScheduledTasks;

    std::atomic<bool> m_scheduled_triggered = false;
    int m_epoll_fd{-1}, m_scheduled_fd{-1};

    std::array<struct epoll_event, max_events> m_events;
};
