#pragma once

#include <coroutine>

class Eventloop;

class Event
{

public:
    struct EventAwaiter;
    friend struct EventAwaiter;
    friend class Eventloop;

    enum Status {
        ERROR,
        TIMEOUT,
        EVENT,
        CLOSED,
    };

    class EventAwaiter {
        friend class Event;
    public:
        auto await_ready() -> bool { return false; }
        auto await_suspend(std::coroutine_handle<> handle) -> void
        {
            m_event.m_handle = handle;
        }
        auto await_resume() -> void {}

    private:
        explicit EventAwaiter(Event& event) : m_event(event) {}
        Event& m_event;
    };

    ~Event() = default;

    auto operator co_await() noexcept -> EventAwaiter { return EventAwaiter{*this}; }

private:
    explicit Event(int fd) : m_fd(fd) {}

    std::coroutine_handle<> m_handle;
    int m_fd{-1};
    enum Status m_status = Status::ERROR;
};
