#pragma once

#include <singleton.h>

#include <string>

#define LOGE(msg) Logger::instance()->error(__FUNCTION__, msg)
#define LOGE_F(msg) \
    Logger::instance()->error(__FILE__, __LINE__, __FUNCTION__, msg)

#define LOGW(msg) Logger::instance()->warn(__FUNCTION__, msg)
#define LOGW_F(msg) \
    Logger::instance()->warn(__FILE__, __LINE__, __FUNCTION__, msg)

#define LOGI(msg) Logger::instance()->info(__FUNCTION__, msg)
#define LOGI_F(msg) \
    Logger::instance()->info(__FILE__, __LINE__, __FUNCTION__, msg)

#define LOGD(msg) Logger::instance()->debug(__FUNCTION__, msg)
#define LOGD_F(msg) \
    Logger::instance()->debug(__FILE__, __LINE__, __FUNCTION__, msg)

#define LOGT(msg) Logger::instance()->trace(__FUNCTION__, msg)
#define LOGT_F(msg) \
    Logger::instance()->trace(__FILE__, __LINE__, __FUNCTION__, msg)

class Logger : public Singleton<Logger>
{
    friend class Singleton<Logger>;

public:
    enum Color {
        BLACK = 0,
        RED = 1,
        GREEN = 2,
        YELLOW = 3,
        BLUE = 4,
        MAGENTA = 5,
        CYAN = 6,
        GRAY_LIGHT = 7,
        DEFAULT = 9,
        GRAY_DARK = 60,
        RED_LIGHT = 61,
        GREEN_LIGHT = 62,
        YELLOW_LIGHT = 63,
        BLUE_LIGHT = 64,
        MAGENTA_LIGHT = 65,
        CYAN_LIGHT = 66,
        WHITE = 67,
    };

    enum Level {
        TRACE = 0,
        DEBUG,
        INFO,
        WARN,
        ERROR,
    };

    void setLogLevel(enum Level level) { mLevel = level; }

    void log(enum Level level, const std::string& msg);
    void log(enum Level level, const std::string& func, const std::string& msg);
    void log(enum Level level, const std::string& file, int line,
             const std::string& func, const std::string& msg);

    void error(const std::string& msg) { log(ERROR, msg); }
    void error(const std::string& func, const std::string& msg)
    {
        log(ERROR, func, msg);
    }
    void error(const std::string& file, int line, const std::string& func,
               const std::string& msg)
    {
        log(ERROR, file, line, func, msg);
    }

    void warn(const std::string& msg) { log(WARN, msg); }
    void warn(const std::string& func, const std::string& msg)
    {
        log(WARN, func, msg);
    }
    void warn(const std::string& file, int line, const std::string& func,
              const std::string& msg)
    {
        log(WARN, file, line, func, msg);
    }

    void info(const std::string& msg) { log(INFO, msg); }
    void info(const std::string& func, const std::string& msg)
    {
        log(INFO, func, msg);
    }
    void info(const std::string& file, int line, const std::string& func,
              const std::string& msg)
    {
        log(INFO, file, line, func, msg);
    }

    void debug(const std::string& msg) { log(DEBUG, msg); }
    void debug(const std::string& func, const std::string& msg)
    {
        log(DEBUG, func, msg);
    }
    void debug(const std::string& file, int line, const std::string& func,
               const std::string& msg)
    {
        log(DEBUG, file, line, func, msg);
    }

    void trace(const std::string& msg) { log(TRACE, msg); }
    void trace(const std::string& func, const std::string& msg)
    {
        log(TRACE, func, msg);
    }
    void trace(const std::string& file, int line, const std::string& func,
               const std::string& msg)
    {
        log(TRACE, file, line, func, msg);
    }

private:
    Logger() = default;

private:
    enum Level mLevel = Level::INFO;
};
