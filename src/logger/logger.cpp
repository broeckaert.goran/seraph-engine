#include "logger.h"

#include <iostream>
#include <list>
#include <map>
#include <ostream>
#include <sstream>

class LogModifier
{
public:
    explicit LogModifier(const std::string& message) : mMessage(message) {}

    LogModifier setForeground(enum Logger::Color color)
    {
        mForeground = color;
        return *this;
    }

    LogModifier setBackground(enum Logger::Color color)
    {
        mBackground = color;
        return *this;
    }

    LogModifier setBold()
    {
        mModifiers.push_back(1);
        return *this;
    }

    LogModifier setUnderline()
    {
        mModifiers.push_back(4);
        return *this;
    }

    friend std::ostream& operator<<(std::ostream& os,
                                    const LogModifier& modifier)
    {
        return os << "\033[" << modifier.modifiers() << "m" << modifier.mMessage
                  << "\033[0m";
    }

private:
    std::string modifiers() const
    {
        std::stringstream ss;

        ss << mForeground + 30 << ";" << mBackground + 40;
        for (auto& modifier : mModifiers) ss << ";" << modifier;

        return ss.str();
    }

private:
    enum Logger::Color mForeground = Logger::DEFAULT;
    enum Logger::Color mBackground = Logger::DEFAULT;

    std::list<unsigned int> mModifiers;

    std::string mMessage;
};

class LogLevel
{
public:
    static LogLevel getLevel(enum Logger::Level level)
    {
        auto levelModifier = levelMap.find(level);
        if (levelModifier != levelMap.end())
            return levelModifier->second;
        else
            return LogLevel(Logger::TRACE);
    }

    friend std::ostream& operator<<(std::ostream& os, const LogLevel& level)
    {
        return os << LogModifier(LogLevel::levelToString(level.mLevel))
                         .setForeground(level.mColor);
    }

private:
    LogLevel(enum Logger::Level level,
             enum Logger::Color color = Logger::DEFAULT)
        : mLevel(level), mColor(color)
    {
    }

private:
    static std::map<enum Logger::Level, LogLevel> levelMap;

    static std::string levelToString(enum Logger::Level level)
    {
        switch (level) {
        case Logger::ERROR:
            return "[Error]";
        case Logger::WARN:
            return "[Warn]";
        case Logger::INFO:
            return "[Info]";
        case Logger::DEBUG:
            return "[Debug]";
        case Logger::TRACE:
            return "[Trace]";
        }

        // Should never be able to get here.
        return "Unknown";
    }

private:
    enum Logger::Level mLevel;
    enum Logger::Color mColor;
};

void Logger::log(enum Logger::Level level, const std::string& msg)
{
    if (level < mLevel) return;

    std::cout << LogLevel::getLevel(level) << " " << msg << std::endl;
}

void Logger::log(enum Logger::Level level, const std::string& func,
                 const std::string& msg)
{
    if (level < mLevel) return;

    std::cout << LogLevel::getLevel(level) << " " << func << " : " << msg
              << std::endl;
}

void Logger::log(enum Logger::Level level, const std::string& file, int line,
                 const std::string& func, const std::string& msg)
{
    if (level < mLevel) return;

    std::cout << "<" << file << ":" << line << "> " << LogLevel::getLevel(level)
              << " " << func << " : " << msg << std::endl;
}

std::map<enum Logger::Level, LogLevel> LogLevel::levelMap = {
    {Logger::ERROR, LogLevel(Logger::ERROR, Logger::RED)},
    {Logger::WARN, LogLevel(Logger::WARN, Logger::YELLOW)},
    {Logger::INFO, LogLevel(Logger::INFO, Logger::GRAY_LIGHT)},
    {Logger::DEBUG, LogLevel(Logger::DEBUG)},
    {Logger::TRACE, LogLevel(Logger::TRACE)},
};
