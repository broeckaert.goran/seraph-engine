#pragma once

#include <functional>
#include <map>

#include "observer.h"

class Subject
{
public:
    virtual ~Subject() = 0;

    virtual void attach(Observer* observer,
                        std::function<void(const Subject*)> handler);
    virtual void detach(Observer* observer);

    virtual void notify() const;

private:
    std::map<Observer*, std::function<void(const Subject*)>> mAttachedObservers;
};
