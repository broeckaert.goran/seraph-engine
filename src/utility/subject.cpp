#include "subject.h"

Subject::~Subject() {}

void Subject::attach(Observer* observer,
                     std::function<void(const Subject*)> handler)
{
    mAttachedObservers.insert({observer, handler});
    // TODO: notify Observer it is attached to this subject
}

void Subject::detach(Observer* observer)
{
    mAttachedObservers.erase(observer);
    // TODO: notify Observer it is detached from this subject
}

void Subject::notify() const
{
    for (auto [observer, handler] : mAttachedObservers) {
        handler(this);
    }
}
