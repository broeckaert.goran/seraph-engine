#pragma once

template <typename T>
class Singleton
{
public:
    static T* instance()
    {
        static T obj;

        return &obj;
    }

protected:
    Singleton() = default;
};
