#pragma once

class Subject;

class Observer
{
public:
    virtual ~Observer() = 0;

    // TODO: Add metadata to remove Observer from all its subscribed Subject
    // when destructed
};
