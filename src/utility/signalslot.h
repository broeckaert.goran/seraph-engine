#pragma once

#include <algorithm>
#include <list>

template <typename... Args> class Signal
{
public:
    virtual ~Signal() { mConnections.clear(); }

    void connect(Signal<Args...>& connection)
    {
        if (std::find(mConnections.begin(), mConnections.end(), &connection) ==
            mConnections.end())
            mConnections.push_back(&connection);
    }

    void disconnect(Signal<Args...>& connection)
    {
        mConnections.remove(&connection);
    }

    void emit(Args... args)
    {
        for (auto connection : mConnections)
            (*connection)(args...);
    }

    void operator()(Args... args) { emit(args...); }

private:
    std::list<Signal<Args...>*> mConnections;
};

template <int N> struct placeholder_template {
};

namespace std
{
template <int N>
struct is_placeholder<placeholder_template<N>> : integral_constant<int, N + 1> {
};
} // namespace std

template <typename... Args> class Slot : public Signal<Args...>
{
public:
    ~Slot() override = default;

    template <typename T>
    Slot(T* obj, void (T::*func)(Args...))
        : mFunc(bind(func, obj))
    {
    }

    void operator()(Args... args) { mFunc(args...); }

private:
    std::function<void(Args...)> mFunc;

    template <typename T, size_t... Is, typename... PlaceHolders>
    auto bind(std::index_sequence<Is...>, void (T::*func)(Args...),
              PlaceHolders&&... args)
    {
        return std::bind(func, std::forward<PlaceHolders>(args)...,
                         placeholder_template<Is>{}...);
    }

    template <typename T, typename... PlaceHolders>
    auto bind(void (T::*func)(Args...), PlaceHolders&&... args)
    {
        return bind(std::make_index_sequence<sizeof...(Args) -
                                             sizeof...(PlaceHolders) + 1>{},
                    func, std::forward<PlaceHolders>(args)...);
    }
};
