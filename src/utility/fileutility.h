#pragma once

#include <sys/stat.h>

#include <string>

inline bool fileExists(const std::string& filepath)
{
    struct stat buffer = {};
    return (stat(filepath.c_str(), &buffer) == 0);
}
