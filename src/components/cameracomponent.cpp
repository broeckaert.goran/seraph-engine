#include "cameracomponent.h"

#include "fbo.h"
#include "projection.h"
#include "shader.h"

CameraComponent::CameraComponent(BaseProjection* projection)
    : mProjectionMatrix(projection->populateMatrix())
    , mShader(nullptr)
    , mOutput(Fbo::getFramebuffer())
{
}

CameraComponent::CameraComponent(BaseProjection* projection,
                                 const BaseShader* shader)
    : mProjectionMatrix(projection->populateMatrix())
    , mShader(shader)
    , mOutput(Fbo::getFramebuffer())
{
}

CameraComponent::CameraComponent(BaseProjection* projection,
                                 const BaseShader* shader, Fbo* output)
    : mProjectionMatrix(projection->populateMatrix())
    , mShader(shader)
    , mOutput(output)
{
}

glm::mat4 CameraComponent::projectionMatrix() const
{
    return mProjectionMatrix;
}

const BaseShader* CameraComponent::shader() const { return mShader; }

const Fbo* CameraComponent::framebuffer() const { return mOutput; }
