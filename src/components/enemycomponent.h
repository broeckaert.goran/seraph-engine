#pragma once

#include <list>
#include <tuple>

#include "component.h"

class EnemyComponent : public Component
{
public:
    enum State {
        IDLE,
        MOVING,
        COMBAT,
        DEAD,
    };

    explicit EnemyComponent(double health);
    ~EnemyComponent() override = default;

private:
    double mHealth;
    enum State mEnemyState = State::IDLE;
};
