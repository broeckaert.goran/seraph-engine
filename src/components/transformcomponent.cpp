#include "transformcomponent.h"

TransformComponent::TransformComponent(glm::vec3 position) : mPosition(position)
{
    mOrientation =
        glm::angleAxis(glm::radians(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
}

TransformComponent::TransformComponent(glm::vec3 position,
                                       glm::quat orientation)
    : mPosition(position), mOrientation(orientation)
{
}

glm::mat4 TransformComponent::getTransform() { return glm::mat4(); }

glm::mat4 TransformComponent::getView() { return glm::mat4(); }

void TransformComponent::transform(glm::vec3 vector) { mPosition += vector; }

void TransformComponent::transform(glm::vec3 direction, float speed)
{
    // Make sure we're using a unit vector
    mPosition += (glm::normalize(direction) * speed);
}

void TransformComponent::rotate(float angle)
{
    mOrientation = glm::rotate(mOrientation, angle, glm::vec3(0, 1, 0));
}

void TransformComponent::rotate(const glm::quat& orientation)
{
    mOrientation *= orientation;
}

void TransformComponent::rotate(const glm::vec3& axis, float angle)
{
    mOrientation = glm::rotate(mOrientation, angle, axis);
}
