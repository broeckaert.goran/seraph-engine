#pragma once

#include <list>
#include <tuple>

#include "component.h"

class ModelComponent : public Component
{
public:
    ModelComponent() = default;
    ~ModelComponent() override = default;

    void addMesh(unsigned int vao, unsigned int count, unsigned int texture);
    void addMesh(
        const std::tuple<unsigned int, unsigned int, unsigned int>& mesh);

    const std::list<std::tuple<unsigned int, unsigned int, unsigned int>>&
    getMeshes() const;

private:
    std::list<std::tuple<unsigned int, unsigned int, unsigned int>> mMeshes;
};
