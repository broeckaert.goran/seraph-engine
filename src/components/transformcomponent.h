#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

#include "component.h"

class TransformComponent : public Component
{
public:
    // Orientation gets initialized with the rotation axis up, and rotated by 0
    // degrees/radians
    explicit TransformComponent(glm::vec3 position);
    TransformComponent(glm::vec3 position, glm::quat orientation);

    ~TransformComponent() override = default;

    // The getView is the inverted getTransform, this naming makes more sense
    // from the camera perspective
    glm::mat4 getTransform();
    glm::mat4 getView();

    // Functions to modify the data
    void transform(glm::vec3 vector);
    void transform(glm::vec3 direction, float speed);

    // Rotate angle with the axis pointing straight up
    void rotate(float angle);
    // Quaternion rotation
    void rotate(const glm::quat& orientation);
    // Rotate angle around axis
    void rotate(const glm::vec3& axis, float angle);

private:
    glm::vec3 mPosition;
    glm::quat mOrientation;
};
