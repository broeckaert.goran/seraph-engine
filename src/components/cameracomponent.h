#pragma once

#include <glm/glm.hpp>

#include "component.h"

class BaseProjection;
class BaseShader;
class Fbo;

class CameraComponent : public Component
{
public:
    explicit CameraComponent(BaseProjection* projection);
    CameraComponent(BaseProjection* projection, const BaseShader* shader);
    CameraComponent(BaseProjection* projection, const BaseShader* shader,
                    Fbo* output);

    ~CameraComponent() override = default;

    glm::mat4 projectionMatrix() const;
    const BaseShader* shader() const;
    const Fbo* framebuffer() const;

private:
    glm::mat4 mProjectionMatrix;
    const BaseShader* mShader;
    Fbo* mOutput;
};
