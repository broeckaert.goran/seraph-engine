#include "modelcomponent.h"

void ModelComponent::addMesh(unsigned int vao, unsigned int count,
                             unsigned int texture)
{
    mMeshes.push_back(std::make_tuple(vao, count, texture));
}

void ModelComponent::addMesh(
    const std::tuple<unsigned int, unsigned int, unsigned int>& mesh)
{
    mMeshes.push_back(mesh);
}

const std::list<std::tuple<unsigned int, unsigned int, unsigned int>>&
ModelComponent::getMeshes() const
{
    return mMeshes;
}
