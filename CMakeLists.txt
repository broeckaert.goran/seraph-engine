cmake_minimum_required(VERSION 3.22)
project(SeraphEngine)

option(ENGINE_TESTS OFF)
option(SeraphEngine_MAJOR_VERSION 0)
option(SeraphEngine_MINOR_VERSION 0)
option(SeraphEngine_PATCH_VERSION 0)
set(SeraphEngine_VERSION ${SeraphEngine_MAJOR_VERSION}.${SeraphEngine_MINOR_VERSION}.${SeraphEngine_PATCH_VERSION})

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

find_package(PkgConfig REQUIRED)

find_package(OpenGL REQUIRED)
find_package(glfw3 REQUIRED)
find_package(glm REQUIRED)
pkg_check_modules(GLEW REQUIRED glew)

add_subdirectory(external/pybind11)

add_subdirectory(src/baseframework)
add_subdirectory(src/components)
add_subdirectory(src/eventloop)
add_subdirectory(src/logger)
add_subdirectory(src/renderer)
add_subdirectory(src/systems)
add_subdirectory(src/utility)

if(${ENGINE_TESTS})
    enable_testing()
    add_subdirectory(external/googletest)
    add_subdirectory(test)
endif()

configure_file(cmake/SeraphEngineConfig.cmake.in "${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/cmake/${PROJECT_NAME}/SeraphEngineConfig.cmake" @ONLY)
configure_file(cmake/SeraphEngineConfigVersion.cmake.in "${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/cmake/${PROJECT_NAME}/SeraphEngineConfigVersion.cmake" @ONLY)

export(PACKAGE ${PROJECT_NAME})
install(FILES "${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/cmake/${PROJECT_NAME}/SeraphEngineConfig.cmake"
              "${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/cmake/${PROJECT_NAME}/SeraphEngineConfigVersion.cmake"
        DESTINATION "lib/cmake/${PROJECT_NAME}" COMPONENT dev)
install(EXPORT "SeraphEngineTargets"
        DESTINATION "lib/cmake/${PROJECT_NAME}" COMPONENT dev)
