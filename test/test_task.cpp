#include "task.h"

#include <gtest/gtest.h>

#include <concepts>
#include <chrono>
#include <thread>

TEST(Task, HelloWorld)
{
    using task_type = Task<std::string>;

    auto h = []() -> task_type { co_return "Hello"; }();
    auto w = []() -> task_type { co_return "World"; }();

    ASSERT_TRUE(h.promise().result().empty());
    ASSERT_TRUE(w.promise().result().empty());

    h.resume(); // task suspends immediately
    w.resume();

    ASSERT_TRUE(h.is_ready());
    ASSERT_TRUE(w.is_ready());

    auto w_value = std::move(w).promise().result();

    ASSERT_TRUE(h.promise().result() == "Hello");
    ASSERT_EQ(w_value, "World");
    ASSERT_TRUE(w.promise().result().empty());
}

TEST(Task, TaskVoid)
{
    using namespace std::chrono_literals;
    using task_type = Task<>;

    auto t = []() -> task_type
    {
        std::this_thread::sleep_for(10ms);
        co_return;
    }();
    t.resume();

    ASSERT_TRUE(t.is_ready());
}

TEST(Task, TaskExceptionThrown)
{
    using task_type = Task<std::string>;

    std::string throw_msg = "I'll be reached";

    auto task = [&]() -> task_type
    {
        throw std::runtime_error(throw_msg);
        co_return "I'll never be reached";
    }();

    task.resume();

    ASSERT_TRUE(task.is_ready());

    bool thrown{false};
    try
    {
        auto value = task.promise().result();
    }
    catch (const std::exception& e)
    {
        thrown = true;
        ASSERT_EQ(e.what(), throw_msg);
    }

    ASSERT_TRUE(thrown);
}

TEST(Task, TaskInTask)
{
    auto outer_task = []() -> Task<>
    {
        auto inner_task = []() -> Task<int>
        {
            std::cerr << "inner_task start\n";
            std::cerr << "inner_task stop\n";
            co_return 42;
        };

        std::cerr << "outer_task start\n";
        auto v = co_await inner_task();
        EXPECT_EQ(v, 42);
        std::cerr << "outer_task stop\n";
    }();

    outer_task.resume(); // all tasks start suspend, kick it off.

    ASSERT_TRUE(outer_task.is_ready());
}

TEST(Task, TaskInTaskInTask)
{
    auto task1 = []() -> Task<>
    {
        std::cerr << "task1 start\n";
        auto task2 = []() -> Task<int>
        {
            std::cerr << "\ttask2 start\n";
            auto task3 = []() -> Task<int>
            {
                std::cerr << "\t\ttask3 start\n";
                std::cerr << "\t\ttask3 stop\n";
                co_return 3;
            };

            auto v2 = co_await task3();
            EXPECT_EQ(v2, 3);

            std::cerr << "\ttask2 stop\n";
            co_return 2;
        };

        auto v1 = co_await task2();
        EXPECT_EQ(v1, 2);

        std::cerr << "task1 stop\n";
    }();

    task1.resume(); // all tasks start suspended, kick it off.

    ASSERT_TRUE(task1.is_ready());
}

TEST(Task, TaskSuspendReturnVoid)
{
    auto task = []() -> Task<void>
    {
        co_await std::suspend_always{};
        co_await std::suspend_never{};
        co_await std::suspend_always{};
        co_await std::suspend_always{};
        co_return;
    }();

    task.resume(); // initial suspend
    ASSERT_FALSE(task.is_ready());

    task.resume(); // first internal suspend
    ASSERT_FALSE(task.is_ready());

    task.resume(); // second internal suspend
    ASSERT_FALSE(task.is_ready());

    task.resume(); // third internal suspend
    ASSERT_TRUE(task.is_ready());
}

TEST(Task, TaskReturnMultipleSuspendInteger)
{
    auto task = []() -> Task<int>
    {
        co_await std::suspend_always{};
        co_await std::suspend_always{};
        co_await std::suspend_always{};
        co_return 11;
    }();

    task.resume(); // initial suspend
    ASSERT_FALSE(task.is_ready());

    task.resume(); // first internal suspend
    ASSERT_FALSE(task.is_ready());

    task.resume(); // second internal suspend
    ASSERT_FALSE(task.is_ready());

    task.resume(); // third internal suspend
    ASSERT_TRUE(task.is_ready());
    ASSERT_EQ(task.promise().result(), 11);
}

TEST(Task, TaskResumeFromPromise)
{
    auto task1 = [&]() -> Task<int>
    {
        std::cerr << "Task ran\n";
        co_return 42;
    }();

    auto task2 = [&]() -> Task<void>
    {
        std::cerr << "Task 2 ran\n";
        co_return;
    }();

    // task.resume();  normal method of resuming

    std::vector<std::coroutine_handle<>> handles;

    handles.emplace_back(std::coroutine_handle<Task<int>::promise_type>::from_promise(task1.promise()));
    handles.emplace_back(std::coroutine_handle<Task<void>::promise_type>::from_promise(task2.promise()));

    auto& coro_handle1 = handles[0];
    coro_handle1.resume();
    auto& coro_handle2 = handles[1];
    coro_handle2.resume();

    ASSERT_TRUE(task1.is_ready());
    ASSERT_TRUE(coro_handle1.done());
    ASSERT_EQ(task1.promise().result(), 42);

    ASSERT_TRUE(task2.is_ready());
    ASSERT_TRUE(coro_handle2.done());
}

TEST(Task, TaskThrowsVoid)
{
    auto task = []() -> Task<void>
    {
        throw std::runtime_error{"I always throw."};
        co_return;
    }();

    ASSERT_NO_THROW(task.resume());
    ASSERT_TRUE(task.is_ready());
    ASSERT_THROW(task.promise().result(), std::runtime_error);
}

TEST(Task, TaskThrowsLValue)
{
    auto task = []() -> Task<int>
    {
        throw std::runtime_error{"I always throw."};
        co_return 42;
    }();

    ASSERT_NO_THROW(task.resume());
    ASSERT_TRUE(task.is_ready());
    ASSERT_THROW(task.promise().result(), std::runtime_error);
}

TEST(Task, TaskThrowsRValue)
{
    struct type
    {
        int m_value;
    };

    auto task = []() -> Task<type>
    {
        type return_value{42};

        throw std::runtime_error{"I always throw."};
        co_return std::move(return_value);
    }();

    task.resume();
    ASSERT_TRUE(task.is_ready());
    ASSERT_THROW(task.promise().result(), std::runtime_error);
}
