#include "syncwait.h"
#include "task.h"
#include "whenall.h"

#include "gtest/gtest.h"
#include <list>
#include <vector>

TEST(WhenAll, SingleTask)
{
    auto make_task = [](uint64_t amount) -> Task<uint64_t> {
        co_return amount;
    };

    auto output_tasks = sync_wait(when_all(make_task(100)));
    ASSERT_EQ(std::tuple_size<decltype(output_tasks)>(), 1);

    uint64_t counter{0};
    std::apply(
        [&counter](auto&&... tasks) -> void {
            ((counter += tasks.return_value()), ...);
        },
        output_tasks);

    ASSERT_EQ(counter, 100);
}

TEST(WhenAll, SingleTaskWithMove)
{
    auto make_task = [](uint64_t amount) -> Task<uint64_t> {
        co_return amount;
    };

    auto t = make_task(100);
    auto output_tasks = sync_wait(when_all(std::move(t)));
    ASSERT_EQ(std::tuple_size<decltype(output_tasks)>(), 1);

    uint64_t counter{0};
    std::apply(
        [&counter](auto&&... tasks) -> void {
            ((counter += tasks.return_value()), ...);
        },
        output_tasks);

    ASSERT_EQ(counter, 100);
}

TEST(WhenAll, MultipleTask)
{
    auto make_task = [](uint64_t amount) -> Task<uint64_t> {
        co_return amount;
    };

    auto output_tasks =
        sync_wait(when_all(make_task(100), make_task(50), make_task(20)));
    ASSERT_EQ(std::tuple_size<decltype(output_tasks)>(), 3);

    uint64_t counter{0};
    std::apply(
        [&counter](auto&&... tasks) -> void {
            ((counter += tasks.return_value()), ...);
        },
        output_tasks);

    ASSERT_EQ(counter, 170);
}

TEST(WhenAll, SingleTaskWithVector)
{
    auto make_task = [](uint64_t amount) -> Task<uint64_t> {
        co_return amount;
    };

    std::vector<Task<uint64_t>> input_tasks;
    input_tasks.emplace_back(make_task(100));

    auto output_tasks = sync_wait(when_all(std::move(input_tasks)));
    ASSERT_EQ(output_tasks.size(), 1);

    uint64_t counter{0};
    for (const auto& task : output_tasks) {
        counter += task.return_value();
    }

    ASSERT_EQ(counter, 100);
}

TEST(WhenAll, MultipleTasksWithVector)
{
    auto make_task = [](uint64_t amount) -> Task<uint64_t> {
        co_return amount;
    };

    std::vector<Task<uint64_t>> input_tasks;
    input_tasks.emplace_back(make_task(100));
    input_tasks.emplace_back(make_task(200));
    input_tasks.emplace_back(make_task(550));
    input_tasks.emplace_back(make_task(1000));

    auto output_tasks = sync_wait(when_all(std::move(input_tasks)));
    ASSERT_EQ(output_tasks.size(), 4);

    uint64_t counter{0};
    for (const auto& task : output_tasks) {
        counter += task.return_value();
    }

    ASSERT_EQ(counter, 1850);
}

TEST(WhenAll, MultipleTasksWithList)
{
    auto make_task = [](uint64_t amount) -> Task<uint64_t> {
        co_return amount;
    };

    std::list<Task<uint64_t>> input_tasks;
    input_tasks.emplace_back(make_task(100));
    input_tasks.emplace_back(make_task(200));
    input_tasks.emplace_back(make_task(550));
    input_tasks.emplace_back(make_task(1000));

    auto output_tasks = sync_wait(when_all(std::move(input_tasks)));
    ASSERT_EQ(output_tasks.size(), 4);

    uint64_t counter{0};
    for (const auto& task : output_tasks) {
        counter += task.return_value();
    }

    ASSERT_EQ(counter, 1850);
}
