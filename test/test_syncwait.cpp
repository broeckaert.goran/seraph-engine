#include "syncwait.h"
#include "task.h"

#include <gtest/gtest.h>
#include <stdexcept>

TEST(SyncWait, IntegerReturn)
{
    auto func = []() -> Task<int> { co_return 11; };

    auto result = sync_wait(func());
    ASSERT_EQ(result, 11);
}

TEST(SyncWait, VoidReturn)
{
    std::string output;

    auto func = [&]() -> Task<void>
    {
        output = "hello from sync_wait<void>\n";
        co_return;
    };

    sync_wait(func());
    ASSERT_EQ(output, "hello from sync_wait<void>\n");
}

TEST(SyncWait, SuspendedCoroutine)
{
    auto answer = []() -> Task<int>
    {
        std::cerr << "\tThinking deep thoughts...\n";
        co_return 42;
    };

    auto await_answer = [&]() -> Task<int>
    {
        std::cerr << "\tStarting to wait for answer.\n";
        auto a = answer();
        std::cerr << "\tGot the coroutine, getting the value.\n";
        auto v = co_await a;
        std::cerr << "\tCoroutine value is " << v << "\n";
        EXPECT_EQ(v, 42);
        v = co_await a;
        std::cerr << "\tValue is still " << v << "\n";
        EXPECT_EQ(v, 42);
        co_return 1337;
    };

    auto output = sync_wait(await_answer());
    ASSERT_EQ(output, 1337);
}

TEST(SyncWait, ExceptionThrown)
{
    auto f = []() -> Task<uint64_t>
    {
        throw std::runtime_error("I always throw!");
        co_return 1;
    };

    ASSERT_THROW(sync_wait(f()), std::runtime_error);
}
